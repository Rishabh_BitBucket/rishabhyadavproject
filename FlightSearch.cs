﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using NUnit.Framework;
using System.Threading;



namespace Thinkbridge
{
    [TestClass]
    public class FlightSearch
    {
        [TestMethod]
        public void SignUp()
        {
            // Creating a string array to hold the languages for further verification with actual result
            String[] ExpectedResult = { "English", "Dutch" };
            // Creating a new instance of a chrome browser
            IWebDriver driver = new ChromeDriver();
            // Writting the code in try catch block to handel exceptions
            try
            {
                // Navigating to the desired url
                driver.Navigate().GoToUrl("http://jt-dev.azurewebsites.net/#/SignUp");
                // Maximising the window after page gets loaded

                driver.Manage().Window.Maximize();
                // Searching for dropdown list and clicking it
                driver.FindElement(By.Id("language")).Click();
                // Creating a new instance of a read only list to hold the items of list
                ReadOnlyCollection<IWebElement> langList = driver.FindElements(By.Id("language"));
                string ActualResult = ""; // String variable to hold value
                // Traversing for each element in the list
                foreach (IWebElement Lang in langList)
                {
                    ActualResult = Lang.Text;
                    // Traversing for each element in expected result array
                    for (int i = 0; i < ExpectedResult.Length; i++)
                    {
                        // comparing the actual result string with each string in the array
                        if ((ActualResult).Contains(ExpectedResult[i]))
                        {
                            Console.WriteLine("Test Case Passed : language is " + ExpectedResult[i]);
                        }
                        else
                        {
                            Console.WriteLine("Test Case Failed : language is Not displayed");
                        }
                    }
                }
                //Clicking the drop down list item to select language
                driver.FindElement(By.LinkText("English")).Click();
                // Entering the name
                driver.FindElement(By.Id("name")).SendKeys("Rishabh Yadav");
                // Entering the organization name
                driver.FindElement(By.Id("orgName")).SendKeys("Rishabh Yadav");
                // Entering email
                driver.FindElement(By.Id("singUpEmail")).SendKeys("rishabh004yadav@gmail.com");
                // Clicking checkbox "I agree to the terms and condition"
                driver.FindElement(By.XPath("//*[@id='content']/div/div[3]/div/section/div[1]/form/fieldset/div[4]/label/span")).Click();
                Thread.Sleep(3000);// Waiting for the checkbox to be clicked and Get started button should be enabled
                                   // Clicking get started button to sign up         
                driver.FindElement(By.XPath("//*[@id='content']/div/div[3]/div/section/div[1]/form/fieldset/div[5]/button")).Click();
                Thread.Sleep(6000);// Waiting for the page to be loaded and show the success message box
                // verifying welcome email has been sent or not -
                string VerifyEmail = driver.FindElement(By.XPath("//*[@id='content']/div/div[3]/div/section/div[1]/form/div/span")).Displayed.ToString();
                if (VerifyEmail.Equals("True"))
                {
                    Console.WriteLine("Test Case Passed : Email sent successfully");
                }
                else
                {
                    Console.WriteLine("Test Case Failed : Unable to send Email");
                }
            }
            // Catching exceptions 
            catch (Exception e)
            {
                Console.WriteLine("Test Case Failed" + e);
            }
            // Closing and quiting the driver
            finally
            {
                driver.Close();
                driver.Quit();
            }
        }

    }
}
